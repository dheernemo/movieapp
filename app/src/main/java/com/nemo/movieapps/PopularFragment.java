package com.nemo.movieapps;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kennyc.view.MultiStateView;
import com.nemo.movieapps.model.ResponseModel;
import com.nemo.movieapps.model.ResultsItem;
import com.nemo.movieapps.network.RetrofitConfig;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class PopularFragment extends Fragment {


    public PopularFragment() {
        // Required empty public constructor
    }

    MultiStateView multiStateView;
    RecyclerView recyclerView;
    List<ResultsItem> listData = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_popular, container, false);

        multiStateView = view.findViewById(R.id.multiStateView);
        recyclerView = view.findViewById(R.id.rv_movie_popular);

        recyclerView.setAdapter(new MovieAdapter(getActivity(), listData));

        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        ambilDataOnline();

        return view;
    }

    private void ambilDataOnline() {
        multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);

        Call<ResponseModel> requestData = RetrofitConfig.getApiServices().ambilDataPopularMovie();
        requestData.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if (response.isSuccessful()) {
                    List<ResultsItem> resultPopular = response.body().getResults();
                    if (resultPopular != null && resultPopular.size() > 0) {
                        listData = response.body().getResults();
                        recyclerView.setAdapter(new MovieAdapter(getActivity(), listData));

                        multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);

                    } else {
                        multiStateView.setViewState(MultiStateView.VIEW_STATE_EMPTY);
                    }
                } else {
                    multiStateView.setViewState(MultiStateView.VIEW_STATE_ERROR);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                multiStateView.setViewState(MultiStateView.VIEW_STATE_ERROR);
            }
        });

    }

}
