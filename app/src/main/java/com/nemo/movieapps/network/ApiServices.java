package com.nemo.movieapps.network;

import com.nemo.movieapps.model.ResponseModel;

import retrofit2.Call;
import retrofit2.http.GET;


public interface ApiServices {

    @GET("3/movie/popular?api_key=aa6fc7542227ccb83b8b8574dadf1685&language=id-ID&page=1")
    Call<ResponseModel> ambilDataPopularMovie();

}
