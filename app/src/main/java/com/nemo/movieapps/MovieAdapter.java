package com.nemo.movieapps;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nemo.movieapps.model.ResultsItem;

import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MyViewHolder> {

    Context context;
    List<ResultsItem> listMovie;

    public MovieAdapter(Context context, List<ResultsItem> listMovie) {
        this.context = context;
        this.listMovie = listMovie;
    }

    // menyambungkan dengan layout movie item
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View view = layoutInflater.inflate(R.layout.movie_item_list, viewGroup, false);

        return new MyViewHolder(view);
    }

    // set value
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        myViewHolder.tvJudulFilm.setText(listMovie.get(position).getTitle());

        Glide
            .with(context)
            .load("https://image.tmdb.org/t/p/w500/" + listMovie.get(position).getPosterPath())
            .into(myViewHolder.ivGambarFilm);


    }

    @Override
    public int getItemCount() {
        return listMovie.size();
    }

    // untuk menyambungkan id dengan yang ada di movie_item_list
    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvJudulFilm;
        ImageView ivGambarFilm;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tvJudulFilm = itemView.findViewById(R.id.tv_title_film);
            ivGambarFilm = itemView.findViewById(R.id.iv_item_gambar_film);
        }
    }
}
